# NGINX with headers-more Module

This is the official [NGINX Docker container](https://hub.docker.com/_/nginx/) with the headers-more extension available as a dynamic extension. To use headers-more add `load_module /usr/local/nginx/modules/ngx_http_headers_more_filter_module.so;` to the start of your `nginx.conf`.  For more documentation on how to use this extension, see the [official git repo](https://github.com/openresty/headers-more-nginx-module).
